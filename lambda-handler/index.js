const Discord = require('discord.js');
const { IncomingWebhook } = require('@slack/webhook');

const client = new Discord.Client();

const discordToken = process.env.DISCORD_TOKEN;
const url = process.env.SLACK_WEBHOOK_URL;

const webhook = new IncomingWebhook(url);

const init = () => new Promise((resolve, reject) => {
    client.on('ready', () => {
        resolve()
    });
})

exports.handler = async function (event) {
    await client.login(discordToken);
    await init()

    const channels = client.channels.cache.array();

    const members = []
    channels.filter(c=>c.type==="voice").forEach(c=>{
        const membersInChannel = c.members.values()        
        members.push(...membersInChannel)
    })

    if (members.length === 0) {
        return {
            statusCode: 200,
            headers: { "Content-Type": "text/plain" },
            body: message
        };
    }

    // 適当な一人選出
    const someone = members[Math.floor(Math.random() * members.length)].displayName

    let message = `いま、${someone}さん他${members.length - 1}人がdiscoにいるよ👀`

    if (members.length === 1) {
        message = `${someone}さんがdiscoでさみしそうにしてるよ👀`
    }

    await webhook.send({
        text: message,
    });

    return {
        statusCode: 200,
        headers: { "Content-Type": "text/plain" },
        body: message
    };
};


