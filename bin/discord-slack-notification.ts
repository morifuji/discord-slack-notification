#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { DiscordSlackNotificationStack } from '../lib/discord-slack-notification-stack';

const app = new cdk.App();
new DiscordSlackNotificationStack(app, 'DiscordSlackNotificationStack');
