import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as targets from '@aws-cdk/aws-events-targets';
import * as events from '@aws-cdk/aws-events';
import { Duration } from '@aws-cdk/core';
const path = require('path');

require('dotenv').config();

export class DiscordSlackNotificationStack extends cdk.Stack {
  
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const lambdaFunction = new lambda.Function(this, 'MyLambda', {
      code: lambda.Code.fromAsset(path.join(__dirname, '..', 'lambda-handler')),
      handler: 'index.handler',
      runtime: lambda.Runtime.NODEJS_12_X,
      timeout: Duration.minutes(1)
    });
    
    if (process.env.DISCORD_TOKEN===undefined || process.env.SLACK_WEBHOOK_URL === undefined) {
      throw new Error("DISCORD_TOKEN or SLACK_WEBHOOK_URL is not set.")
    }

    lambdaFunction.addEnvironment("DISCORD_TOKEN", process.env.DISCORD_TOKEN)
    lambdaFunction.addEnvironment("SLACK_WEBHOOK_URL", process.env.SLACK_WEBHOOK_URL)

    const rule = new events.Rule(this, 'Rule', {
      schedule: events.Schedule.rate(Duration.minutes(15))
    });

    rule.addTarget(new targets.LambdaFunction(lambdaFunction));
  }
}
